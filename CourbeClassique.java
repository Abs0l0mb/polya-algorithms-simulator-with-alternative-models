package projet_mathsV2.src;
import java.awt.*;


import javax.swing.JComponent;


public class CourbeClassique extends JComponent 
{
	public int nbTirages;
	public int nbUrnes;
	
	
	public CourbeClassique(int nbTirages, int nbUrnes)
	{
		this.nbTirages = nbTirages;
		this.nbUrnes = nbUrnes;
	}
	
	
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
        
		// creer le fond
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		
		// tracer l'axe
		
		g.setColor(Color.BLACK);
		g.drawLine(1, 1, 1, getHeight());
		g.drawLine(1, getHeight() -1 , getWidth(), getHeight() - 1);
		
		// tracer la courbe
		
		g.setColor(Color.RED);
		
		
		//double step = 10.0;
		
			// valeur n�cessaires � l'algorithme
		
		double nbBlanc = 1;
        double nbNoir = 1;
        double ratio = nbBlanc/(nbNoir+nbBlanc);
        double res;
        
    	//point de d�part
		int oldX = 0;
		int oldY = getHeight()/2;
		
        int k = this.nbTirages;
        int nbTirage =  this.nbUrnes;
        for (int ii = 0 ; ii < nbTirage ; ii++) {
        	//System.out.println("urne lanc�e");
        	for(int i = 0; i < k; i++)
        	{
        		res = Math.random();
        		if(res < ratio)
        		{
        			nbBlanc++;
        		}
        		else
        		{
        			nbNoir++;
        		}
        		ratio = nbBlanc/(nbNoir+nbBlanc);
        		double y = ratio * getHeight();
        		double x = (nbNoir + nbBlanc) -2;
            
        		if(i==0) {
            
        			g.drawLine(oldX, oldY, xToPixel(x,k), yToPixel(y));
        		}else {
        			g.drawLine(xToPixel(oldX,k), yToPixel(oldY), xToPixel(x,k), yToPixel(y));
        		}
        		//System.out.println( "oldX " +oldX +" OldY "+ oldY +" newX " + xToPixel(x,k) + " newY" + yToPixel(y) +" y sans topixel"+ y);
        		//System.out.println(x);
            
        		oldX = (int) x;
        		oldY = (int) y;
        	}
        	oldX = 0;
    		oldY = getHeight()/2;
    		nbBlanc = 1;
            nbNoir = 1;
            ratio = nbBlanc/(nbNoir+nbBlanc);
        }
	}
	
	
	private int xToPixel( double x, int k ) {
        return (int)((getWidth()/k)*x);
    }

    private int yToPixel( double y ) {
        return (int)( getHeight() - y );
    }
}