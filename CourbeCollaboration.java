package projet_mathsV2.src;
import java.awt.*;


import javax.swing.JComponent;


public class CourbeCollaboration extends JComponent 
{
	public int nbTirages;
	public int nbUrnes;

	
	
	public CourbeCollaboration(int nbTirages, int nbUrnes)
	{
		this.nbTirages = nbTirages;
		this.nbUrnes = nbUrnes;
	}
	
	
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
        
		// creer le fond
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		
		// tracer l'axe
		
		g.setColor(Color.BLACK);
		g.drawLine(1, 1, 1, getHeight());
		g.drawLine(1, getHeight() -1 , getWidth(), getHeight() - 1);
		
		// tracer la courbe
		
		g.setColor(Color.RED);
		
		

		
			// valeur n�cessaires � l'algorithme
		
		double nbBlanc = 1;
        double nbNoir = 1;
        double ratio = nbBlanc/(nbNoir+nbBlanc);
        double res;
        int nbOccurBlancs = 1;
        int nbOccurNoirs = 1;
    	//point de d�part
		int oldX = 0;
		int oldY = getHeight()/2;
		
        int k = this.nbTirages;
        int nbTirage =  this.nbUrnes;
        for (int ii = 0 ; ii < nbTirage ; ii++) {
        	for(int i = 0; i < k; i++)
        	{
        		res = Math.random();
        		if(res < ratio)
        		{
        			nbOccurBlancs++;
        			nbBlanc = Math.sqrt(nbOccurBlancs);
        		}
        		else
        		{
        			nbOccurNoirs++;
        			nbNoir = Math.sqrt(nbOccurNoirs);
        		}
        		ratio = nbBlanc/(nbNoir+nbBlanc);
        		double ratiox = (double)i/ (double)k;
        		double y = ratio * getHeight();
        		double x = ratiox*getWidth();
            
        		if(i==0) {
            
        			g.drawLine(oldX, oldY, xToPixel(x,k), yToPixel(y));
        		}else {
        			g.drawLine(xToPixel(oldX,k), yToPixel(oldY), xToPixel(x,k), yToPixel(y));
        		}

        		oldX = (int) x;
        		oldY = (int) y;
        	}
        	oldX = 0;
    		oldY = getHeight()/2;
    		nbBlanc = 1;
            nbNoir = 1;
            ratio = nbBlanc/(nbNoir+nbBlanc);
            nbOccurBlancs = 1;
            nbOccurNoirs = 1;
        }
	}
	
	
	private int xToPixel( double x, int k ) {
        return (int)(x+1);
    }

    private int yToPixel( double y ) {
        return (int)( getHeight() - y );
    }
}