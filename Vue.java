package projet_mathsV2.src;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Vue extends JPanel {
	
    private JButton tracer = new JButton("Tracer le graphe");
    public JTextField nbTirage = new JTextField();
    public JTextField nbSeries = new JTextField();
    public JTextField facteurHandicap = new JTextField();
    public BorderLayout BL = new BorderLayout(10,10);
    public JButton classique = new JButton("Classique");
    public JButton renforce = new JButton("Renforc�");
    public JButton avantage = new JButton("Avantage");
    public JButton collaboration = new JButton("Collaboration");

    public Vue() {
    	
    	Controleur monControleur = new Controleur(this);
        
        this.setLayout(BL);
        
        JPanel panelHaut = new JPanel();
        JPanel panelBas = new JPanel(new FlowLayout());

        // redimensionnage des textfield
        Dimension dimension = new Dimension(150,30);
        this.nbTirage.setPreferredSize(dimension);
        this.nbSeries.setPreferredSize(dimension);
        this.facteurHandicap.setPreferredSize(dimension);

        //mise en place des boutons de choix de courbe
        panelHaut.setLayout(new FlowLayout());
        
        panelHaut.add(new JLabel("Choix de votre type d'algorithme de P�lya : "));
        panelHaut.add(this.classique);
        panelHaut.add(this.renforce);
        panelHaut.add(this.avantage);
        panelHaut.add(this.collaboration);

        
        // mise en place des labels descriptif des parametres de la fonction
        panelBas.add(new JLabel("Nombre de tirages : "));
        panelBas.add(nbTirage);

        panelBas.add(new JLabel("Nombre de s�ries : "));
        panelBas.add(nbSeries);

        panelBas.add(new JLabel("Suite de renforcement : "));
        panelBas.add(facteurHandicap);
        
        panelBas.add(tracer);
   

        this.add(panelHaut, BorderLayout.NORTH);
        this.add(panelBas, BorderLayout.SOUTH);

      
        // ajout de listener sur chaque bouton
        classique.addActionListener(monControleur);
        renforce.addActionListener(monControleur);
        avantage.addActionListener(monControleur);
        collaboration.addActionListener(monControleur);
        tracer.addActionListener(monControleur);
        

    }
    

    
}