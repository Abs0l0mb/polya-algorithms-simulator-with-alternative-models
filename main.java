package projet_mathsV2.src;
import java.awt.Graphics;

import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import javax.swing.*;

public class main {
    public static void main(String[]args) {
    	try {
			UIManager.setLookAndFeel( new NimbusLookAndFeel() );
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        int w = 1080;
        int h = 720;
        JFrame fenetre = new JFrame("Simulation d'urnes de P�lya et variantes");
        Vue vue = new Vue();
        fenetre.setSize(w, h);
        fenetre.setResizable(false);
        fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        fenetre.setVisible(true);
        fenetre.getContentPane().add(vue);
        
    }
    
}