package projet_mathsV2.src;
import java.awt.*;
import java.awt.event.*;

import javax.swing.JButton;

public class Controleur implements ActionListener
{
	private enum Etat {CLASSIQUE , RENFORCE , AVANTAGE , COLLABORATION };
	private Vue maVue;
	private Etat etat;
	public CourbeClassique maCourbeClassique;
	public CourbeRenforcee maCourbeRenforcee;
	public CourbeAvantage maCourbeAvantage;
	public CourbeCollaboration maCourbeCollaboration;
	public Controleur(Vue v)
	{
		this.etat = Etat.CLASSIQUE;
		this.maVue = v;
		this.maCourbeClassique = new CourbeClassique(0, 0);
		this.maCourbeAvantage = new CourbeAvantage(0,0,0.0);
		this.maCourbeRenforcee = new CourbeRenforcee(0,0,0.0);
		this.maCourbeCollaboration = new CourbeCollaboration(0,0);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b = (JButton) e.getSource();
		if (b.getText().equals("Renforc�")) {
			this.etat = Etat.RENFORCE;	
			this.maVue.remove(this.maCourbeAvantage);
			this.maVue.remove(this.maCourbeClassique);
			this.maVue.remove(this.maCourbeCollaboration);
		}
		else if (b.getText().equals("Avantage")) {
			this.etat = Etat.AVANTAGE;
			this.maVue.remove(this.maCourbeRenforcee);
			this.maVue.remove(this.maCourbeClassique);
			this.maVue.remove(this.maCourbeCollaboration);
			
		}
		else if (b.getText().equals("Collaboration")) {
			this.etat = Etat.COLLABORATION;
			this.maVue.remove(this.maCourbeAvantage);
			this.maVue.remove(this.maCourbeClassique);
			this.maVue.remove(this.maCourbeRenforcee);
		}
		else if (b.getText().equals("Classique")) {
			this.etat = Etat.CLASSIQUE;
			this.maVue.remove(this.maCourbeAvantage);
			this.maVue.remove(this.maCourbeRenforcee);
			this.maVue.remove(this.maCourbeCollaboration);
		}
	
		switch (etat) {
			case CLASSIQUE :
				System.out.println("etat classique");
				this.maVue.facteurHandicap.setEditable(false);
				this.maVue.classique.setEnabled(false);
				this.maVue.renforce.setEnabled(true);
				this.maVue.collaboration.setEnabled(true);
				this.maVue.avantage.setEnabled(true);
			
				if (b.getText().equals("Tracer le graphe")) {
					System.out.println("tracer");
					int nbTirage = Integer.parseInt(this.maVue.nbTirage.getText());
					int nbSeries = Integer.parseInt(this.maVue.nbSeries.getText());
				
					this.maCourbeClassique.nbTirages = nbTirage;
					this.maCourbeClassique.nbUrnes= nbSeries;

				
			
					this.maVue.add(this.maCourbeClassique, this.maVue.BL.CENTER);
		
					this.maVue.revalidate();
					this.maVue.repaint();
					} else {}
				
				break;
		
		
		
		
			
				
			case RENFORCE :
				System.out.println("etat renforce");
				this.maVue.facteurHandicap.setEditable(true);
				this.maVue.renforce.setEnabled(false);
				this.maVue.classique.setEnabled(true);
				this.maVue.collaboration.setEnabled(true);
				this.maVue.avantage.setEnabled(true);
				if (b.getText().equals("Tracer le graphe")) {
					System.out.println("tracer");
					int nbTirage = Integer.parseInt(this.maVue.nbTirage.getText());
					int nbSeries = Integer.parseInt(this.maVue.nbSeries.getText());
					double facteurAvantage = Double.parseDouble(this.maVue.facteurHandicap.getText());
				
					this.maCourbeRenforcee.nbTirages = nbTirage;
					this.maCourbeRenforcee.nbUrnes= nbSeries;
					this.maCourbeRenforcee.facteurAvantage= facteurAvantage;

					
			
					this.maVue.add(this.maCourbeRenforcee, this.maVue.BL.CENTER);
		
					this.maVue.revalidate();
					this.maVue.repaint();
					} else {}
			
				break;
			
				
			case AVANTAGE :
				System.out.println("etat avantage");
				this.maVue.facteurHandicap.setEditable(true);
				this.maVue.avantage.setEnabled(false);
				this.maVue.renforce.setEnabled(true);
				this.maVue.collaboration.setEnabled(true);
				this.maVue.classique.setEnabled(true);
				if (b.getText().equals("Tracer le graphe")) {
					System.out.println("tracer");
					int nbTirage = Integer.parseInt(this.maVue.nbTirage.getText());
					int nbSeries = Integer.parseInt(this.maVue.nbSeries.getText());
					double facteurAvantage = Double.parseDouble(this.maVue.facteurHandicap.getText());
				
					this.maCourbeAvantage.nbTirages = nbTirage;
					this.maCourbeAvantage.nbUrnes= nbSeries;
					this.maCourbeAvantage.facteurAvantage= facteurAvantage;

					
			
					this.maVue.add(this.maCourbeAvantage, this.maVue.BL.CENTER);
		
					this.maVue.revalidate();
					this.maVue.repaint();
					} else {}
			
				break;
				
			case COLLABORATION:
				this.maVue.facteurHandicap.setEditable(false);
				System.out.println("etat colaboration");
				this.maVue.collaboration.setEnabled(false);
				this.maVue.renforce.setEnabled(true);
				this.maVue.classique.setEnabled(true);
				this.maVue.avantage.setEnabled(true);
				if (b.getText().equals("Tracer le graphe")) {
					System.out.println("tracer");
					int nbTirage = Integer.parseInt(this.maVue.nbTirage.getText());
					int nbSeries = Integer.parseInt(this.maVue.nbSeries.getText());
					
				
					this.maCourbeCollaboration.nbTirages = nbTirage;
					this.maCourbeCollaboration.nbUrnes= nbSeries;
					

					
			
					this.maVue.add(this.maCourbeCollaboration, this.maVue.BL.CENTER);
		
					this.maVue.revalidate();
					this.maVue.repaint();
					} else {}
	
			break;
				
	}
	
}
}